Description
-----------
Allow cloning of entities created with module ECK.
It works similar as node_clone.

Installation
------------
To install this module, do the following:

1. Extract the tar ball that you downloaded from Drupal.org.

2. Upload the entire directory and all its contents to your modules directory.

Configuration
-------------
To enable and configure this module do the following:

1. Go to Admin -> Modules, and enable ECK Clone.

2. Go to Admin -> Configuration -> Content Authoring -> ECK Clone, and make
   any necessary configuration changes. 


